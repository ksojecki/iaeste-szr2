<?php
App::uses('AppModel', 'Model');
/**
 * RecruitmentsInCurrentGlobalRecruitment Model
 *
 * @property LocalCommittee $LocalCommittee
 * @property RecruitmentStatus $RecruitmentStatus
 */
class RecruitmentsInCurrentGlobalRecruitment extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'recruitments_in_current_global_recruitment';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'local_committee_id';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'recruitment_status_human_name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'local_committee_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'local_committee_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'LocalCommittee' => array(
			'className' => 'LocalCommittee',
			'foreignKey' => 'local_committee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'RecruitmentStatus' => array(
			'className' => 'RecruitmentStatus',
			'foreignKey' => 'recruitment_status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
