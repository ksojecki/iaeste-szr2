<?php
App::uses('AppController', 'Controller');
/**
 * MainController
 *
 * @property Dashboard $Dashboard
 * @property PaginatorComponent $Paginator
 */
abstract class MainController extends AppController {

/**
 * Components
 *
 * @var array
 */
 
	public $menuDefinition = array(
		'options'=>array('class'=>'nav'),
		'items'=>
		array(
			array('title'=>'Dashboard', 'url'=> array('controller'=>'dashboard', 'action'=>'index')),
			array('title'=>'Kandydaci', 'url'=> array('controller'=>'users', 'action'=>'index')),
			array('title'=>'Użytkownicy', 'url'=> array('controller'=>'users', 'action'=>'index')),
			)
		);
	
	public function __construct (CakeRequest $request = null , CakeResponse $response = null ) {
		parent::__construct($request, $response);
		$this->set('menu', $this->menuDefinition);
	}
	
	public $helpers = array('Menu');
}
