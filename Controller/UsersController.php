<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	public $paginate = array(
		'limit' => 20 );
	
	var $uses = array('User', 'LocalCommittee');
	
	public function beforeFilter() 
	{
        parent::beforeFilter();
		
        $this->Auth->allow('login');
		$this->Auth->allow('register');
		$this->Auth->allow('registersuccess');
    }

/**
 * index method
 *
 * @return void
 */
	public function index() 
	{
		$this->User->recursive = 0;
		$this->LocalCommittee->recursive = -1;
		$this->Paginator->settings = $this->paginate;
		
		if(isset($this->request->named['lc']) && is_numeric($this->request->named['lc'])){
			$this->Paginator->settings = array_merge($this->Paginator->settings, array(
					'conditions' => array('local_committee_id' => $this->request->named['lc'])
				)	
			);
			
			$currentLC = $this->LocalCommittee->find('first', array(
				'conditions'=> array(
					$this->LocalCommittee->primaryKey => $this->request->named['lc']
					)
				));
				
			$this->set('currentLC', $currentLC['LocalCommittee']);
		}
		
		$this->set('localCommittees', $this->LocalCommittee->find('all'));
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) 
	{
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) 
	{
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		
		$localCommittees = $this->User->LocalCommittee->find('list');
		$userRoles = $this->User->UserRole->find('list');
		
		$this->set(compact('localCommittees', 'userRoles'));
	}

/**
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */	
	public function login()
	{
		$this->layout = "system";
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				if($this->Auth->user('activated') == 1){
					return $this->redirect($this->Auth->redirect());
				} 
				$this->Session->setFlash(__('Twoje konto nie jest jeszcze aktywne!'));
				$this->Auth->logout();
			} else {
				$this->Session->setFlash(__('Niepoprawny login lub hasło!'));
			}
		}
	}
	
	public function registerSuccess()
	{
		$this->layout = "system";
		$userName = $this->Session->read('user_name');
		$userEmail =  $this->Session->read('user_email');
		$this->set(compact('userName', 'userEmail'));
	}
	
	public function register()
	{
		$this->layout = "system";
		
		if ($this->request->is('post')) {
			if ($this->data['User']['password'] == $this->data['User']['password_retype']){
				$this->request->data['User']['password'] = $this->Auth->password($this->data['User']['password']);
				$this->request->data['User']['password_retype'] = $this->Auth->password($this->data['User']['password_retype']);
				
				$this->User->create();
				
				if ($this->User->save($this->request->data)) {
					$this->Session->write('user_name', $this->data['User']['name']);
					$this->Session->write('user_email', $this->data['User']['email']);
					
					return $this->redirect(array('controller'=>'Users' ,'action' => 'registersuccess'));
				} else {
					$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
				}
			}
		}
		
		$localCommittees = $this->User->LocalCommittee->find('list');

		$this->set(compact('localCommittees', 'userRoles'));
	}
	
	public function logout()
	{
		return $this->redirect($this->Auth->logout());
	}
	
	public function profile()
	{
		$user = $this->Session->read('Auth.User');
		$this->set('profile', $user);
		$this->set('_serialize', array('profile'));
	}
	
	public function changePassword()
	{
		
	}

}


