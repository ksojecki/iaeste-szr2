<?php
App::uses('AppController', 'Controller');

class CandidatesController extends AppController{

    var $uses = array('User', 'LocalCommittee', 'Recruitment', 'GlobalRecruitment');

    public function index(){
        if(isset($this->request->named['lc']) && is_numeric($this->request->named['lc'])){
            $currentLC = $this->LocalCommittee->find('first', array(
                'conditions'=> array(
                    $this->LocalCommittee->primaryKey => $this->request->named['lc']
                )
            ));
            $this->set('currentLC', $currentLC['LocalCommittee']);
        }

        $this->set('localCommittees', $this->LocalCommittee->find('all'));
    }
} 