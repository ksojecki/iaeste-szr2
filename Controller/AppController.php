<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	/**
 * Components
 *
 * @var array
 */
   public $components = array(
        'Session',
        'RequestHandler',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'dashboard', 'action' => 'index'),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),

			'authenticate' => array(
				'Form' => array(
					'fields' => array('username' => 'email')
				)
			)
        )
    );
	
 
	public $menuDefinition = array(
		'options'=>array('class'=> array('nav', 'navbar-nav')),
		'items'=>
		array(
			array('title'=>'Rekrutacja', 'url'=> array('controller'=>'recruitments', 'action'=>'index')),
			array('title'=>'Kandydaci', 'url'=> array('controller'=>'candidates', 'action'=>'index')),
			array('title'=>'Pytania', 'url'=> array('controller'=>'questions', 'action'=>'index')),
			array('title'=>'Użytkownicy', 'url'=> array('controller'=>'users', 'action'=>'index')),
			)
		);
	
	public function __construct (CakeRequest $request = null , CakeResponse $response = null ) {
		parent::__construct($request, $response);
		$this->set('menu', $this->menuDefinition);
	}
	
	
	public $helpers = array('Menu', 'Html');
}
