<?php
App::uses('AppController', 'Controller');
/**
 * Recruitments Controller
 *
 * @property Recruitment $Recruitment
 * @property PaginatorComponent $Paginator
 */
class RecruitmentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	var $uses = array('User', 'LocalCommittee', 'Recruitment', 'GlobalRecruitment', 'RecruitmentsInCurrentGlobalRecruitment');
	
	public function index(){
		$this->RecruitmentsInCurrentGlobalRecruitment->recursive= -1;
		
		$this->set('globalRecruitment', $this->GlobalRecruitment->find('first', array('order' => 'id DESC')));
		$selectedRecruitment = $this->RecruitmentsInCurrentGlobalRecruitment->find('all');
		
		$this->set('recruitments', $this->RecruitmentsInCurrentGlobalRecruitment->find('all'));
		$this->set('selectedRecruitment', $selectedRecruitment);
	}
}
