<?php
App::uses('AppController', 'Controller');
/**
 * LocalCommittees Controller
 *
 * @property LocalCommittee $LocalCommittee
 * @property PaginatorComponent $Paginator
 */
class LocalCommitteesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->LocalCommittee->recursive = 0;
		$this->set('localCommittees', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->LocalCommittee->exists($id)) {
			throw new NotFoundException(__('Invalid local committee'));
		}
		$options = array('conditions' => array('LocalCommittee.' . $this->LocalCommittee->primaryKey => $id));
		$this->set('localCommittee', $this->LocalCommittee->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->LocalCommittee->create();
			if ($this->LocalCommittee->save($this->request->data)) {
				$this->Session->setFlash(__('The local committee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The local committee could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->LocalCommittee->exists($id)) {
			throw new NotFoundException(__('Invalid local committee'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->LocalCommittee->save($this->request->data)) {
				$this->Session->setFlash(__('The local committee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The local committee could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('LocalCommittee.' . $this->LocalCommittee->primaryKey => $id));
			$this->request->data = $this->LocalCommittee->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->LocalCommittee->id = $id;
		if (!$this->LocalCommittee->exists()) {
			throw new NotFoundException(__('Invalid local committee'));
		}
		
		$this->request->onlyAllow('post', 'delete');
		
		if ($this->LocalCommittee->delete()) {
			$this->Session->setFlash(__('The local committee has been deleted.'));
		} else {
			$this->Session->setFlash(__('The local committee could not be deleted. Please, try again.'));
		}
		
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->LocalCommittee->recursive = 0;
		$this->set('localCommittees', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->LocalCommittee->exists($id)) {
			throw new NotFoundException(__('Invalid local committee'));
		}
		$options = array('conditions' => array('LocalCommittee.' . $this->LocalCommittee->primaryKey => $id));
		$this->set('localCommittee', $this->LocalCommittee->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->LocalCommittee->create();
			if ($this->LocalCommittee->save($this->request->data)) {
				$this->Session->setFlash(__('The local committee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The local committee could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->LocalCommittee->exists($id)) {
			throw new NotFoundException(__('Invalid local committee'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->LocalCommittee->save($this->request->data)) {
				$this->Session->setFlash(__('The local committee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The local committee could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('LocalCommittee.' . $this->LocalCommittee->primaryKey => $id));
			$this->request->data = $this->LocalCommittee->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->LocalCommittee->id = $id;
		if (!$this->LocalCommittee->exists()) {
			throw new NotFoundException(__('Invalid local committee'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->LocalCommittee->delete()) {
			$this->Session->setFlash(__('The local committee has been deleted.'));
		} else {
			$this->Session->setFlash(__('The local committee could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
