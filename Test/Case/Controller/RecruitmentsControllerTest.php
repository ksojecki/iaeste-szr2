<?php
App::uses('RecruitmentsController', 'Controller');

/**
 * RecruitmentsController Test Case
 *
 */
class RecruitmentsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.recruitment',
		'app.local_commitee',
		'app.status',
		'app.global_recruitment',
		'app.candidate_response',
		'app.candidate',
		'app.faculty',
		'app.university',
		'app.local_committee',
		'app.user',
		'app.user_role',
		'app.candidate_history',
		'app.candidate_status'
	);

}
