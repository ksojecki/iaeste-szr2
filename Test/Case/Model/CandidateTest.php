<?php
App::uses('Candidate', 'Model');

/**
 * Candidate Test Case
 *
 */
class CandidateTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.candidate',
		'app.faculty',
		'app.university',
		'app.local_committee',
		'app.user',
		'app.user_role',
		'app.candidate_history',
		'app.candidate_status',
		'app.candidate_response',
		'app.global_recruitment',
		'app.recruitment',
		'app.local_commitee',
		'app.status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Candidate = ClassRegistry::init('Candidate');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Candidate);

		parent::tearDown();
	}

}
