<?php
App::uses('Recruitment', 'Model');

/**
 * Recruitment Test Case
 *
 */
class RecruitmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.recruitment',
		'app.local_committee',
		'app.university',
		'app.faculty',
		'app.candidate',
		'app.candidate_history',
		'app.candidate_status',
		'app.user',
		'app.user_role',
		'app.candidate_response',
		'app.global_recruitment',
		'app.status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Recruitment = ClassRegistry::init('Recruitment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Recruitment);

		parent::tearDown();
	}

}
