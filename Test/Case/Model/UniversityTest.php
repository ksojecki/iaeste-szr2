<?php
App::uses('University', 'Model');

/**
 * University Test Case
 *
 */
class UniversityTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.university',
		'app.local_committee',
		'app.user',
		'app.user_role',
		'app.candidate_history',
		'app.candidate',
		'app.faculty',
		'app.candidate_response',
		'app.global_recruitment',
		'app.recruitment',
		'app.local_commitee',
		'app.status',
		'app.candidate_status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->University = ClassRegistry::init('University');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->University);

		parent::tearDown();
	}

}
