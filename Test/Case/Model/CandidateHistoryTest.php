<?php
App::uses('CandidateHistory', 'Model');

/**
 * CandidateHistory Test Case
 *
 */
class CandidateHistoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.candidate_history',
		'app.candidate',
		'app.faculty',
		'app.university',
		'app.local_committee',
		'app.user',
		'app.user_role',
		'app.candidate_response',
		'app.global_recruitment',
		'app.recruitment',
		'app.local_commitee',
		'app.status',
		'app.candidate_status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CandidateHistory = ClassRegistry::init('CandidateHistory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CandidateHistory);

		parent::tearDown();
	}

}
