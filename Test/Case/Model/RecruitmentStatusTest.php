<?php
App::uses('RecruitmentStatus', 'Model');

/**
 * RecruitmentStatus Test Case
 *
 */
class RecruitmentStatusTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.recruitment_status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RecruitmentStatus = ClassRegistry::init('RecruitmentStatus');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RecruitmentStatus);

		parent::tearDown();
	}

}
