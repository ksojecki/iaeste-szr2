<?php
App::uses('CandidateStatus', 'Model');

/**
 * CandidateStatus Test Case
 *
 */
class CandidateStatusTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.candidate_status',
		'app.candidate_history',
		'app.candidate',
		'app.faculty',
		'app.university',
		'app.local_committee',
		'app.user',
		'app.user_role',
		'app.candidate_response',
		'app.global_recruitment',
		'app.recruitment',
		'app.local_commitee',
		'app.status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CandidateStatus = ClassRegistry::init('CandidateStatus');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CandidateStatus);

		parent::tearDown();
	}

}
