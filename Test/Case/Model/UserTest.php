<?php
App::uses('User', 'Model');

/**
 * User Test Case
 *
 */
class UserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user',
		'app.local_committee',
		'app.university',
		'app.faculty',
		'app.candidate',
		'app.candidate_history',
		'app.candidate_status',
		'app.candidate_response',
		'app.global_recruitment',
		'app.recruitment',
		'app.local_commitee',
		'app.status',
		'app.user_role'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->User = ClassRegistry::init('User');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->User);

		parent::tearDown();
	}

}
