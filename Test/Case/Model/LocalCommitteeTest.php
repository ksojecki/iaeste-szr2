<?php
App::uses('LocalCommittee', 'Model');

/**
 * LocalCommittee Test Case
 *
 */
class LocalCommitteeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.local_committee',
		'app.recruitment',
		'app.status',
		'app.global_recruitment',
		'app.candidate_response',
		'app.candidate',
		'app.faculty',
		'app.university',
		'app.candidate_history',
		'app.candidate_status',
		'app.user',
		'app.user_role'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->LocalCommittee = ClassRegistry::init('LocalCommittee');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->LocalCommittee);

		parent::tearDown();
	}

}
