<?php
App::uses('RecruitmentsInCurrentGlobalRecruitment', 'Model');

/**
 * RecruitmentsInCurrentGlobalRecruitment Test Case
 *
 */
class RecruitmentsInCurrentGlobalRecruitmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.recruitments_in_current_global_recruitment',
		'app.local_committee',
		'app.recruitment',
		'app.status',
		'app.global_recruitment',
		'app.candidate_response',
		'app.candidate',
		'app.faculty',
		'app.university',
		'app.candidate_history',
		'app.candidate_status',
		'app.user',
		'app.user_role',
		'app.recruitment_status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RecruitmentsInCurrentGlobalRecruitment = ClassRegistry::init('RecruitmentsInCurrentGlobalRecruitment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RecruitmentsInCurrentGlobalRecruitment);

		parent::tearDown();
	}

}
