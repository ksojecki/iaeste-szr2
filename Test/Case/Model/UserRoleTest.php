<?php
App::uses('UserRole', 'Model');

/**
 * UserRole Test Case
 *
 */
class UserRoleTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_role',
		'app.user',
		'app.local_committee',
		'app.university',
		'app.faculty',
		'app.candidate',
		'app.candidate_history',
		'app.candidate_status',
		'app.candidate_response',
		'app.global_recruitment',
		'app.recruitment',
		'app.local_commitee',
		'app.status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserRole = ClassRegistry::init('UserRole');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserRole);

		parent::tearDown();
	}

}
