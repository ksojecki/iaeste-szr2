<?php
App::uses('GlobalRecruitment', 'Model');

/**
 * GlobalRecruitment Test Case
 *
 */
class GlobalRecruitmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.global_recruitment',
		'app.candidate_response',
		'app.candidate',
		'app.faculty',
		'app.university',
		'app.local_committee',
		'app.user',
		'app.user_role',
		'app.candidate_history',
		'app.candidate_status',
		'app.recruitment',
		'app.local_commitee',
		'app.status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->GlobalRecruitment = ClassRegistry::init('GlobalRecruitment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->GlobalRecruitment);

		parent::tearDown();
	}

}
