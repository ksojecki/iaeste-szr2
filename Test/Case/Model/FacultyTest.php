<?php
App::uses('Faculty', 'Model');

/**
 * Faculty Test Case
 *
 */
class FacultyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.faculty',
		'app.university',
		'app.local_committee',
		'app.user',
		'app.user_role',
		'app.candidate_history',
		'app.candidate',
		'app.candidate_response',
		'app.global_recruitment',
		'app.recruitment',
		'app.local_commitee',
		'app.status',
		'app.candidate_status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Faculty = ClassRegistry::init('Faculty');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Faculty);

		parent::tearDown();
	}

}
