<?php
App::uses('CandidateResponse', 'Model');

/**
 * CandidateResponse Test Case
 *
 */
class CandidateResponseTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.candidate_response',
		'app.global_recruitment',
		'app.recruitment',
		'app.local_commitee',
		'app.status',
		'app.candidate',
		'app.faculty',
		'app.university',
		'app.local_committee',
		'app.user',
		'app.user_role',
		'app.candidate_history',
		'app.candidate_status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CandidateResponse = ClassRegistry::init('CandidateResponse');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CandidateResponse);

		parent::tearDown();
	}

}
