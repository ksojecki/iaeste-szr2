<?php
/**
 * RecruitmentFixture
 *
 */
class RecruitmentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'local_committee_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 64),
		'status_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'global_recruitment_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_polish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'local_committee_id' => 1,
			'status_id' => 1,
			'global_recruitment_id' => 1
		),
	);

}
