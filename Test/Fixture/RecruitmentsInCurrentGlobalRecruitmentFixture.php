<?php
/**
 * RecruitmentsInCurrentGlobalRecruitmentFixture
 *
 */
class RecruitmentsInCurrentGlobalRecruitmentFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'recruitments_in_current_global_recruitment';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'local_committee_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'local_committee_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 64, 'collate' => 'utf8_polish_ci', 'charset' => 'utf8'),
		'recruitment_status_id' => array('type' => 'biginteger', 'null' => false, 'default' => '0', 'length' => 11),
		'recruitment_status_name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'utf8_polish_ci', 'charset' => 'utf8'),
		'recruitment_status_human_name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 64, 'collate' => 'utf8_polish_ci', 'charset' => 'utf8'),
		'indexes' => array(
			
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'local_committee_id' => 1,
			'local_committee_name' => 'Lorem ipsum dolor sit amet',
			'recruitment_status_id' => '',
			'recruitment_status_name' => 'Lorem ipsum dolor sit amet',
			'recruitment_status_human_name' => 'Lorem ipsum dolor sit amet'
		),
	);

}
