<?php
/**
 * CandidateFixture
 *
 */
class CandidateFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 128, 'collate' => 'utf8_polish_ci', 'charset' => 'utf8'),
		'surname' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 128, 'collate' => 'utf8_polish_ci', 'charset' => 'utf8'),
		'faculty_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'email' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 128),
		'phone' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 128, 'collate' => 'utf8_polish_ci', 'charset' => 'utf8'),
		'year_of_study' => array('type' => 'integer', 'null' => false, 'default' => null),
		'field_of_study' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 128, 'collate' => 'utf8_polish_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_polish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'surname' => 'Lorem ipsum dolor sit amet',
			'faculty_id' => 1,
			'email' => 1,
			'phone' => 'Lorem ipsum dolor sit amet',
			'year_of_study' => 1,
			'field_of_study' => 'Lorem ipsum dolor sit amet'
		),
	);

}
