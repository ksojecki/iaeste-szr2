<?php $this->Html->script('recruitments.js', array('inline' => false)); ?>

<div class="row">
<div class="col-md-3">
	<?php echo $this->element('recruitments_menu'); ?>
</div>
<div class="col-md-9">
	<div class="btn-group dropdown pull-right" style="padding-left: 10px;">
	  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	    <span>Otwarta</span>
	    <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu">
	    <li><a href="#">Zamknij</a></li>
	    <li><a href="#">Zmień nazwę</a></li>
	  </ul>
	</div>
	<h2 style="margin-top: 0px;"><span class="label label-success pull-right" title="45 zarejestrowanych">45</span>AGH</h2>
	<div class="panel panel-default">
		  <div class="panel-heading">Ankiety dziennie</div>
			  <div class="panel-body">
				<div id="chart_div"></div>
			  </div>
			</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
			  <div class="panel-heading">Względem wydziałów</div>
			  <div class="panel-body">
				<p>FTIMS <span class="label label-default pull-right">5</span></p>
				<div class="progress">
				  <div class="progress-bar progress-bar-success" style="width: 35%"></div>
				</div>
				<p>ARCH<span class="label label-default pull-right">5</span></p>
				<div class="progress">
				  <div class="progress-bar progress-bar-primary" style="width: 35%"></div>
				</div>
				<p>Strona internetowa<span class="label label-default pull-right">5</span></p>
				<div class="progress">
				  <div class="progress-bar progress-bar-info" style="width: 35%"></div>
				</div>
				<p>Plakaty, ulotki<span class="label label-default pull-right">5</span></p>
				<div class="progress">
				  <div class="progress-bar progress-bar-warning" style="width: 35%"></div>
				</div>
				<p>Inne<span class="label label-default pull-right">5</span></p>
				<div class="progress">
				  <div class="progress-bar progress-bar-danger" style="width: 35%"></div>
				</div>
			  </div>
			</div>
		</div>
			<div class="col-md-6">
			<div class="panel panel-default">
			  <div class="panel-heading">Źródła</div>
			  <div class="panel-body">
				<p>Znajomi <span class="label label-default pull-right">5</span></p>
				<div class="progress">
				  <div class="progress-bar progress-bar-success" style="width: 35%"></div>
				</div>
				<p>Facebook<span class="label label-default pull-right">5</span></p>
				<div class="progress">
				  <div class="progress-bar progress-bar-primary" style="width: 35%"></div>
				</div>
				<p>Strona internetowa<span class="label label-default pull-right">5</span></p>
				<div class="progress">
				  <div class="progress-bar progress-bar-info" style="width: 35%"></div>
				</div>
				<p>Plakaty, ulotki<span class="label label-default pull-right">5</span></p>
				<div class="progress">
				  <div class="progress-bar progress-bar-warning" style="width: 35%"></div>
				</div>
				<p>Inne<span class="label label-default pull-right">5</span></p>
				<div class="progress">
				  <div class="progress-bar progress-bar-danger" style="width: 35%"></div>
				</div>
			  </div>
			</div>
		</div>
	</div>
</div>
</div>



