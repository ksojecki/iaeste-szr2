	<div class="list-group">
		<?php $classForMenuItem = "list-group-item"; ?>
		
  		<?php echo $this->html->link('Globalna', array('controller' => 'Recruitments', 'action' => 'globalSettings'), array('class' => $classForMenuItem)); ?>
	<?php foreach ($recruitments as $recruitment): ?>
		<?php $content = 
			'<span class="badge pull-right">'.
			h($recruitment['RecruitmentsInCurrentGlobalRecruitment']['recruitment_status_human_name']).
			'</span>'.
			h($recruitment['RecruitmentsInCurrentGlobalRecruitment']['local_committee_name']); ?>
		<?php echo $this->html->link(
		$content, 
		array('controller' => 'Recruitments', 'action' => 'index', 'id' => $recruitment['RecruitmentsInCurrentGlobalRecruitment']['local_committee_id']), 
		array('class' => $classForMenuItem,  'escape' => false)); ?>
	<?php endforeach; ?>
  	</div>