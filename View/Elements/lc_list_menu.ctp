<div class="list-group">
    <?php
    $isCurrentMenuItemActive = !isset($currentLC);
    $classForMenuItem = ($isCurrentMenuItemActive) ? array('list-group-item', 'active') : array('list-group-item');
    ?>
    <?php echo $this->html->link('Wszyscy', array('controller' => $this->params['controller'], 'action' => 'index'), array('class' => $classForMenuItem)); ?>
    <?php foreach ($localCommittees as $localCommittee): ?>
        <?php
        $isCurrentMenuItemActive = (isset($currentLC) && $currentLC['id'] == $localCommittee['LocalCommittee']['id']);
        $classForMenuItem = ($isCurrentMenuItemActive) ? array('list-group-item', 'active') : array('list-group-item');
        ?>
        <?php echo $this->html->link($localCommittee['LocalCommittee']['name'], array('controller' => $this->params['controller'], 'action' => 'index', 'lc' => $localCommittee['LocalCommittee']['id']), array('class' => $classForMenuItem)); ?>
    <?php endforeach; ?>
</div>
