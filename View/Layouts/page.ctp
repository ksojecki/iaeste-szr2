<?php
/**
 *
 * PHP 5
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

?>
<!DOCTYPE html>
<html>
  <head>
	<?php echo $this->Html->charset(); ?>
    <title><?php echo $title_for_layout; ?></title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css(array('bstr', 'bstr.resp'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
  </head>
  <body>
	<?php echo $this->Session->flash(); ?>
	<?php echo $this->fetch('content'); ?>
    <script src="http://code.jquery.com/jquery.js"></script>
	
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>