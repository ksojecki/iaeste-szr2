<?php
/**
 *
 * PHP 5
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

?>
<!DOCTYPE html>
<html>
  <head>
	<?php echo $this->Html->charset(); ?>
    <title><?php echo $title_for_layout; ?></title>
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
	<script src="http://codeorigin.jquery.com/jquery-2.0.3.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array('layout'));

		echo $this->fetch('script');
		echo $this->fetch('meta');
		echo $this->fetch('css');
	?>
	
	<style>
	body{
		padding-top: 10px;
	}
	
	.jumbotron {
        margin: 80px 40px;
		
      }
	.jumbotron h1 {
		font-size: 100px;
		line-height: 1;
	}
	.jumbotron .lead {
		font-size: 24px;
		line-height: 1.25;
	}
	.jumbotron .btn {
		font-size: 21px;
		padding: 14px 24px;
	}
	.loginPage{
		padding-top: 200px;
	}
	</style>
	
  </head>
  <body>
		<div class="container">
			<?php echo $this->fetch('content'); ?>
		<div class="container">
		
	</div>
  </body>
</html>