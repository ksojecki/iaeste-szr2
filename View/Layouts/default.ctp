<?php
/**
 *
 * PHP 5
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$user = $this->Session->read('Auth.User');
 
 
?>
<!DOCTYPE html>
<html>
  <head>
	<?php echo $this->Html->charset(); ?>
    <title><?php echo $title_for_layout; ?></title>
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
	<script src="http://codeorigin.jquery.com/jquery-2.0.3.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
    <script src="http://knockoutjs.com/downloads/knockout-3.0.0.debug.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array('layout'));

		echo $this->fetch('script');
		echo $this->fetch('meta');
		echo $this->fetch('css');
	?>
	
	
	   <!--Load the AJAX API-->

	
  </head>
  <body>
	<div class="container">
		
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">IAESTE SZR 2.0</a>
			</div>
			<?php print $this->Menu->render($menu); ?>
			
			<ul class="nav navbar-nav navbar-right ">
				<?php if(!empty($user)) { ?>
				<li class="dropdown">

					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					<span class="glyphicon glyphicon-user"></span> <?php echo $user['name']." ".$user['surname']; ?>
					<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><?php echo $this->Html->link('Profil', '/users/profile', array('tabindex' => '-1')); ?></li>
						<li class="divider"></li>
						<li><?php echo $this->Html->link('Wyloguj', '/users/logout', array('tabindex' => '-1')); ?></li>
					</ul>
				</li>
				<?php } ?>
				
			</ul>
			</div>
		</nav>
		
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
	</div>
  </body>
</html>