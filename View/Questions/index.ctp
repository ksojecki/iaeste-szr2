<?php $this->Html->script('questions.js', array('inline' => false)); ?>

<div class="row">
    <div class="col-md-2">
        <?= $this->element('lc_list_menu'); ?>
    </div>
    <div class="col-md-10">
        <h2 style="margin-top: 0px;">Pytania</h2>
        <table class="table table-bordered table-striped vertical-middle">
            <tr>
                <th>Pytanie</th>
                <th>Typ</th>
                <th>Opcje</th>
                <th></th>
            </tr>

                <tr>
                    <td><textarea class="form-control"></textarea></td>
                    <td><select><option>1</option><option>2</option><option>3</option></select></td>
                    <td></td>
                    <td><button role="button">usun</button></td>
                </tr>
        </table>
    </div>
</div>



