<div class="row">
	<div class="col-md-2">
        <?= $this->element('lc_list_menu'); ?>
	</div>
	<div class="col-md-10">
		<ul class="pagination" style="margin-top: 0; float: right">
		  <li><?php echo $this->Paginator->prev(__('&laquo;'), array('tag' => false, 'escape'=> false)); ?></li>
		  <li><span><?php echo $this->Paginator->counter(
				'Łącznie użytkowników: {:count}. Aktuanie wyświetlono: {:current}'
			);?></span></li>
		  <?php echo $this->Paginator->numbers(array('separator' => '', 'tag'=> 'li', 'currentTag'=> 'span')); ?>
		  <li><?php echo $this->Paginator->next(__('&raquo;'), array('tag' => false, 'escape'=> false)); ?></li>
		</ul>
		
		<h2 style="margin-top: 0px"><?php echo (!isset($currentLC)) ? "Wszyscy użytkownicy" : "Użytkownicy z ".$currentLC['name']; ?></h2>
		<table class="table table-bordered table-striped vertical-middle">
		<tr>
			<th>Imię</th>
			<th>Nazwisko</th>
			<th>Email</th>
			<th>Rola</th>
			<th>Komitet</th>
			<th>Aktywny</th>
		</tr>
		<?php foreach ($users as $user): ?>
			<tr>
				<td><?php echo h($user['User']['name']); ?></td>
				<td><?php echo h($user['User']['surname']); ?></td>
				<td><?php echo h($user['User']['email']); ?></td>
				<td><?php echo h($user['UserRole']['human_name']); ?></td>
				<td><?php echo h($user['LocalCommittee']['name']); ?></td>
				<td><?php echo ($user['User']['activated'])? '<a class="btn btn-sm btn-success disabled">Tak</a>': '<a class="btn btn-sm btn-default" href="#">Aktywuj</a>'; ?></td>
			</tr>
		<?php endforeach; ?>
		</table>
		<ul class="pagination" style="float: right">
		  <li><?php echo $this->Paginator->prev(__('&laquo;'), array('tag' => false, 'escape'=> false)); ?></li>
		  <li><span><?php echo $this->Paginator->counter(
				'Łącznie użytkowników: {:count}. Aktuanie wyświetlono: {:current}'
			);?></span></li>
		  <?php echo $this->Paginator->numbers(array('separator' => '', 'tag'=> 'li', 'currentTag'=> 'span')); ?>
		  <li><?php echo $this->Paginator->next(__('&raquo;'), array('tag' => false, 'escape'=> false)); ?></li>
		</ul>
	</div>


</div>



