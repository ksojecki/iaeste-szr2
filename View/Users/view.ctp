<div class="users view">
<h2><?php echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Local Committee'); ?></dt>
		<dd>
			<?php echo $this->Html->link($user['LocalCommittee']['name'], array('controller' => 'local_committees', 'action' => 'view', $user['LocalCommittee']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User Role'); ?></dt>
		<dd>
			<?php echo $this->Html->link($user['UserRole']['name'], array('controller' => 'user_roles', 'action' => 'view', $user['UserRole']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Surname'); ?></dt>
		<dd>
			<?php echo h($user['User']['surname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Local Committees'), array('controller' => 'local_committees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Local Committee'), array('controller' => 'local_committees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List User Roles'), array('controller' => 'user_roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Role'), array('controller' => 'user_roles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Candidate Histories'), array('controller' => 'candidate_histories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Candidate History'), array('controller' => 'candidate_histories', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Candidate Histories'); ?></h3>
	<?php if (!empty($user['CandidateHistory'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Candidate Id'); ?></th>
		<th><?php echo __('Candidate Status Id'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Timestamp'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['CandidateHistory'] as $candidateHistory): ?>
		<tr>
			<td><?php echo $candidateHistory['id']; ?></td>
			<td><?php echo $candidateHistory['candidate_id']; ?></td>
			<td><?php echo $candidateHistory['candidate_status_id']; ?></td>
			<td><?php echo $candidateHistory['description']; ?></td>
			<td><?php echo $candidateHistory['timestamp']; ?></td>
			<td><?php echo $candidateHistory['user_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'candidate_histories', 'action' => 'view', $candidateHistory['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'candidate_histories', 'action' => 'edit', $candidateHistory['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'candidate_histories', 'action' => 'delete', $candidateHistory['id']), null, __('Are you sure you want to delete # %s?', $candidateHistory['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Candidate History'), array('controller' => 'candidate_histories', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
