<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend>Register</legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('surname');
		echo $this->Form->input('local_committee_id');
		echo $this->Form->input('email');
		echo $this->Form->input('password');
		echo $this->Form->input('password_retype', array('type' => 'password'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>

<?php echo $this->element('sql_dump'); ?>
</div>
