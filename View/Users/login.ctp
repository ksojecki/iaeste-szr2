			<div class="row loginPage">
				<div class="col-md-6">
					<h2>IAESTE SZR</h2>
					<p>Jeśli posiadasz konto w systemie zaloguj się używając formatki po prawej.</p>
					<p>Jeśli nie posiadasz, załóż je teraz. Będzie ono aktywne dopiero po potwierdzeniu przez Koordynatora Lokalnego z twojego komitetu.</p>
					
					<?php echo $this->html->link("Zarejestruj się", array('controller' => 'Users', 'action' => 'register'), array('class' => array('btn', 'btn-success'))); ?>
				</div>
				<div class="col-md-offset-2 col-md-4">
					<div class="well">
						<?php echo $this->Form->create('User') ?>
							<legend>Logowanie</legend>
							<?php 
							$flash = $this->Session->flash();
							if (trim($flash)!= false){ ?>
								<div class="alert alert-error">
								<?php echo $flash; ?>
								</div>
							<?php }?>
							<div class="form-group">
							  <?php echo $this->Form->input('email', array('label' => false, 'placeholder' => 'Email')); ?>
							</div>
							<div class="form-group">
							  <?php echo $this->Form->input('password', array('label' => false, 'placeholder' => 'Hasło')); ?>
							</div>
							<div class="form-group">
							</div>
						<?php echo $this->Form->end(__('Login')); ?>
					</div>
				</div>
			</div>




