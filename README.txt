Siemanko :)

W projekcie używamy composera. Zainstaluj go sobie według instrukcji tutaj

https://getcomposer.org/

Następnia odpal polecenie composer install w tym katalogu. Jakby nie było katalogu Vendors (composer możę się wywalić), 
to dodaj ten katalog :)

SQL z dumpel bazy danych znajduje sie w pliku szr2.sql. Odpal :) Następnie skopiuj plik Config/database.php.default i zmień nazwę na Config/database.php
i odpowiednio go uzupełnij.

Do odpalenia instalacji polecam php w wersji 5.5. Wtedy w CMD piszemy php -S localhost:8000 i powinien odpalić sie serwer pod tym adresem.
Oczywiście musisz być w katalogu iaest-szr2/webroot

Pozdro, Kamil <kamil@sojecki.pl>