google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Dzień', 'Zarejestrowani'],
        ['3 paź',  2],
        ['4 paź',  5],
        ['5 paź',  10],
        ['6 paź',  23]
    ]);

    var options = {
        hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0}
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}