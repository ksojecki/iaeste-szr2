-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas wygenerowania: 08 Gru 2013, 23:09
-- Wersja serwera: 5.5.32
-- Wersja PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `szr2`
--
CREATE DATABASE IF NOT EXISTS `szr2` DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci;
USE `szr2`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `candidates`
--

CREATE TABLE IF NOT EXISTS `candidates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `surname` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `email` int(128) NOT NULL,
  `phone` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `year_of_study` int(11) NOT NULL,
  `field_of_study` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `candidate_histories`
--

CREATE TABLE IF NOT EXISTS `candidate_histories` (
  `id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `candidate_status_id` int(11) NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `candidate_responses`
--

CREATE TABLE IF NOT EXISTS `candidate_responses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `global_recruitment_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `candidate_id` int(11) NOT NULL,
  `text_01` text COLLATE utf8_polish_ci,
  `text_02` text COLLATE utf8_polish_ci,
  `text_03` text COLLATE utf8_polish_ci,
  `text_04` text COLLATE utf8_polish_ci,
  `text_05` text COLLATE utf8_polish_ci,
  `text_06` text COLLATE utf8_polish_ci,
  `text_07` text COLLATE utf8_polish_ci,
  `text_08` text COLLATE utf8_polish_ci,
  `text_09` text COLLATE utf8_polish_ci,
  `text_10` text COLLATE utf8_polish_ci,
  `varchar_01` varchar(256) COLLATE utf8_polish_ci DEFAULT NULL,
  `varchar_02` varchar(256) COLLATE utf8_polish_ci DEFAULT NULL,
  `varchar_03` varchar(256) COLLATE utf8_polish_ci DEFAULT NULL,
  `varchar_04` varchar(256) COLLATE utf8_polish_ci DEFAULT NULL,
  `varchar_05` varchar(256) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `candidate_statuses`
--

CREATE TABLE IF NOT EXISTS `candidate_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_polish_ci NOT NULL,
  `human_name` varchar(64) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=7 ;

--
-- Zrzut danych tabeli `candidate_statuses`
--

INSERT INTO `candidate_statuses` (`id`, `name`, `human_name`) VALUES
(1, 'registered', 'Zarejestrowany'),
(2, 'meeting_setted_up', 'Umówiony na spotkanie'),
(3, 'meeting_helded', 'Po spotkaniu'),
(4, 'meeting_not_helded', 'Spotkanie nie odbyło się'),
(5, 'rejected', 'Odrzucony'),
(6, 'accepted', 'Przyjęty');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `faculties`
--

CREATE TABLE IF NOT EXISTS `faculties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `university_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=204 ;

--
-- Zrzut danych tabeli `faculties`
--

INSERT INTO `faculties` (`id`, `university_id`, `name`) VALUES
(1, 1, 'Fizyki Technicznej i Matematyki Stosowanej'),
(2, 1, 'Chemiczny'),
(3, 1, 'Elektroniki, Telekomunikacji i Informatyki'),
(4, 1, 'Elektrotechniki i Automatyki'),
(5, 1, 'Architektury'),
(6, 1, 'Inżynierii Lądowej i Środowiska'),
(7, 1, 'Mechaniczny'),
(8, 1, 'Oceanotechniki i Okrętownictwa'),
(9, 1, 'Zarządzania i Ekonomii'),
(19, 4, 'Górnictwa i Geoinżynierii'),
(20, 4, 'Inżynierii Metali i Informatyki Przemysłowej'),
(21, 4, 'Wydział Elektrotechniki, Automatyki, Informatyki i Inżynierii Biomedycznej'),
(22, 4, 'Inżynierii Mechanicznej i Robotyki'),
(23, 4, 'Geologii, Geofizyki i Ochrony Środowiska'),
(24, 4, 'Geodezji Górniczej i Inżynierii Środowiska'),
(25, 4, 'Inżynierii Materiałowej i Ceramiki'),
(26, 4, 'Odlewnictwa'),
(27, 4, 'Metali Nieżelaznych'),
(28, 4, 'Wiertnictwa, Nafty i Gazu'),
(29, 4, 'Zarządzania'),
(30, 4, 'Energetyki i Paliw'),
(31, 4, 'Fizyki i Informatyki Stosowanej'),
(32, 4, 'Matematyki Stosowanej'),
(33, 4, 'Humanistyczny'),
(35, 2, 'Inżynierii Mechanicznej i Informatyki'),
(36, 2, 'Inżynierii Procesowej, Materiałowej i Fizyki Stosowanej'),
(37, 2, 'Elektryczny'),
(38, 2, 'Budownictwa'),
(39, 2, 'Inżynierii i Ochrony Środowiska'),
(40, 2, 'Zarządzania'),
(41, 3, 'Architektury'),
(42, 3, 'Automatyki, Elektroniki i Informatyki'),
(43, 3, 'Budownictwa'),
(44, 3, 'Chemiczny'),
(45, 3, 'Elektryczny'),
(46, 3, 'Górnictwa i Geologii'),
(47, 3, 'Inżynierii Biomedycznej'),
(48, 3, 'Inżynierii Środowiska i Energetyki'),
(49, 3, 'Matematyki Stosowanej'),
(50, 3, 'Mechaniczny Technologiczny'),
(51, 3, 'Inżynierii Materiałowej i Metalurgii'),
(52, 3, 'Organizacji i Zarządzania'),
(53, 3, 'Transportu'),
(54, 3, 'Kolegium Języków Obcych'),
(55, 5, 'Architektury'),
(56, 5, 'Fizyki, Matematyki i Informatyki'),
(57, 5, 'Inżynierii Elektrycznej i Komputerowej'),
(58, 5, 'Inżynierii i Technologii Chemicznej'),
(59, 5, 'Inżynierii Lądowej'),
(60, 5, 'Inżynierii Środowiska'),
(61, 5, 'Mechaniczny'),
(62, 6, 'Mechaniczny'),
(63, 6, 'Elektrotechniki, Elektroniki, Informatyki i Automatyki'),
(64, 6, 'Chemiczny'),
(65, 6, 'Technologii Materiałowych i Wzornictwa Tekstyliów'),
(66, 6, 'Biotechnologii i Nauk o Żywności'),
(67, 6, 'Budownictwa, Architektury i Inżynierii Środowiska'),
(68, 6, 'Fizyki Technicznej, Informatyki i Matematyki Stosowanej'),
(69, 6, 'Organizacji i Zarządzania'),
(70, 6, 'Inżynierii Procesowej i Ochrony Środowiska'),
(81, 7, 'Filologiczny'),
(82, 7, 'Filozoficzno-Historyczny'),
(83, 7, 'Biologii i Ochrony Środowiska'),
(84, 7, 'Prawa i Administracji'),
(85, 7, 'Ekonomiczno-Socjologiczny'),
(86, 7, 'Nauk o Wychowaniu'),
(87, 7, 'Zarządzania'),
(88, 7, 'Matematyki i Informatyki'),
(89, 7, 'Studiów Międzynarodowych i Politologicznych'),
(90, 7, 'Nauk Geograficznych'),
(91, 7, 'Fizyki i Informatyki Stosowanej'),
(92, 7, 'Chemii'),
(93, 8, 'Farmaceutyczny'),
(94, 8, 'Nauk Biomedycznych i Kształcenia Podyplomowego'),
(95, 8, 'Lekarski'),
(96, 8, 'Nauk o Zdrowiu'),
(97, 8, 'Wojskowo-Lekarski'),
(98, 9, 'Budownictwa i Architektury'),
(99, 9, 'Elektrotechniki i Informatyki'),
(100, 9, 'Mechaniczny'),
(101, 9, 'Inżynierii Środowiska'),
(102, 9, 'Podstaw Techniki'),
(103, 9, 'Zarządzania'),
(104, 10, 'Bioinżynierii Zwierząt'),
(105, 10, 'Biologii'),
(106, 10, 'Geodezji i Gospodarki Przestrzennej'),
(107, 10, 'Humanistyczny'),
(108, 10, 'Kształtowania Środowiska i Rolnictwa'),
(109, 10, 'Matematyki i Informatyki'),
(110, 10, 'Medycyny Weterynaryjnej'),
(111, 10, 'Nauk Ekonomicznych'),
(112, 10, 'Nauk Medycznych'),
(113, 10, 'Nauk Społecznych'),
(114, 10, 'Nauk Technicznych'),
(115, 10, 'Nauki o Żywności'),
(116, 10, 'Ochrony Środowiska i Rybactwa'),
(117, 10, 'Prawa i Administracji'),
(118, 10, 'Sztuki'),
(119, 10, 'Teologii'),
(120, 11, 'Architektury'),
(121, 11, 'Budownictwa i Inżynierii Środowiska'),
(122, 11, 'Budowy Maszyn i Zarządzania'),
(123, 11, 'Elektroniki i Telekomunikacji'),
(124, 11, 'Elektryczny'),
(125, 11, 'Fizyki Technicznej'),
(126, 11, 'Informatyki'),
(127, 11, 'Inżynierii Zarządzania'),
(128, 11, 'Maszyn Roboczych i Transportu'),
(129, 11, 'Technologii Chemicznej'),
(130, 12, 'Budownictwa i Inżynierii Środowiska'),
(131, 12, 'Budowy Maszyn i Lotnictwa'),
(132, 12, 'Chemiczny'),
(133, 12, 'Elektrotechniki i Informatyki'),
(134, 12, 'Matematyki i Fizyki Stosowanej'),
(135, 12, 'Zarządzania'),
(136, 13, 'Biotechnologii i Hodowli Zwierząt'),
(137, 13, 'Budownictwa i Architektury'),
(138, 13, 'Elektryczny'),
(139, 13, 'Ekonomiczny'),
(140, 13, 'Informatyki'),
(141, 13, 'Inżynierii Mechanicznej i Mechatroniki'),
(142, 13, 'Kształtowania Środowiska i Rolnictwa'),
(143, 13, 'Nauk o Żywności i Rybactwa'),
(144, 13, 'Techniki Morskiej'),
(145, 13, 'Technologii i Inżynierii Chemicznej'),
(146, 14, 'Administracji i Nauk Społecznych'),
(147, 14, 'Architektury'),
(148, 14, 'Chemiczny'),
(149, 14, 'Elektroniki i Technik Informacyjnych'),
(150, 14, 'Elektryczny'),
(151, 14, 'Fizyki'),
(152, 14, 'Geodezji i Kartografii'),
(153, 14, 'Inżynierii Chemicznej i Procesowej'),
(154, 14, 'Inżynierii Lądowej'),
(155, 14, 'Inżynierii Materiałowej'),
(156, 14, 'Inżynierii Produkcji'),
(157, 14, 'Inżynierii Środowiska'),
(158, 14, 'Matematyki i Nauk Informacyjnych'),
(159, 14, 'Mechaniczny Energetyki i Lotnictwa'),
(160, 14, 'Mechatroniki'),
(161, 14, 'Samochodów i Maszyn Roboczych'),
(162, 14, 'Transportu'),
(163, 14, 'Zarządzania'),
(164, 14, 'Szkoła Nauk Technicznych i Społecznych Politechniki Warszawskiej w Płocku'),
(165, 15, 'Architektury'),
(166, 15, 'Budownictwa Lądowego i Wodnego'),
(167, 15, 'Chemiczny'),
(168, 15, 'Elektroniki'),
(169, 15, 'Elektryczny'),
(170, 15, 'Geoinżynierii, Górnictwa i Geologii'),
(171, 15, 'Inżynierii Środowiska'),
(172, 15, 'Informatyki i Zarządzania'),
(173, 15, 'Mechaniczno-Energetyczny'),
(174, 15, 'Mechaniczny'),
(175, 15, 'Podstawowych Problemów Techniki'),
(176, 15, 'Elektroniki Mikrosystemów i Fotoniki'),
(177, 16, 'Wydział Biotechnologii '),
(178, 16, 'Wydział Chemii'),
(179, 16, 'Wydział Filologiczny'),
(180, 16, 'Wydział Fizyki i Astronomii'),
(181, 16, 'Wydział Matematyki i Informatyki'),
(182, 16, 'Wydział Nauk Biologicznych'),
(183, 16, 'Wydział Nauk Historycznych i Pedagogicznych'),
(184, 16, 'Wydział Nauk o Ziemi i Kształtowania Środowiska'),
(185, 16, 'Wydział Nauk Społecznych'),
(186, 16, 'Wydział Prawa, Administracji i Ekonomii'),
(187, 16, 'Kolegium Międzywydziałowych Indywidualnych Studiów Humanistycznych'),
(188, 16, 'Międzywydziałowe Studium Ochrony Środowiska'),
(189, 17, 'Rolnictwa i Biologii'),
(190, 17, 'Medycyny Weterynaryjnej'),
(191, 17, 'Leśny'),
(192, 17, 'Ogrodnictwa i Architektury Krajobrazu'),
(193, 17, 'Technologii Drewna'),
(194, 17, 'Nauk o Zwierzętach'),
(195, 17, 'Nauk o Żywnośći'),
(196, 17, 'Nauk o Żywieniu Człowieka i Konsumpcji'),
(197, 17, 'Inżynierii Produkcji'),
(198, 17, 'Nauk Ekonomicznych'),
(199, 17, 'Nauk Humanistycznych'),
(200, 17, 'Zastosowań  Informatyki i Matematyki'),
(201, 17, 'Wydział Budownictwa i Inżynierii Środowiska'),
(202, 17, 'Kierunki Międzywydziałowe (Biotechnologia, Gospodarka przestrzenna, Towaroznawstwo, Ochrona Środowiska)'),
(203, 4, 'Wydział Informatyki, Elektroniki i Telekomunikacji');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `global_recruitments`
--

CREATE TABLE IF NOT EXISTS `global_recruitments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(11) COLLATE utf8_polish_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `global_recruitments`
--

INSERT INTO `global_recruitments` (`id`, `name`, `date`) VALUES
(2, 'Lato 2009', '2013-09-26 21:44:58'),
(3, 'wiosna 2010', '2013-09-28 11:41:40');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `local_committees`
--

CREATE TABLE IF NOT EXISTS `local_committees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=15 ;

--
-- Zrzut danych tabeli `local_committees`
--

INSERT INTO `local_committees` (`id`, `name`) VALUES
(1, 'PGD'),
(2, 'PWA'),
(3, 'AGH'),
(4, 'PWR'),
(5, 'PK'),
(6, 'PSL'),
(7, 'UMED Łódź'),
(8, 'PLO'),
(9, 'PLU'),
(10, 'UWR'),
(11, 'ZUT'),
(12, 'ULO'),
(13, 'PPO'),
(14, 'PRZ');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `recruitments`
--

CREATE TABLE IF NOT EXISTS `recruitments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `local_committee_id` int(64) NOT NULL,
  `status_id` int(11) NOT NULL,
  `global_recruitment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=6 ;

--
-- Zrzut danych tabeli `recruitments`
--

INSERT INTO `recruitments` (`id`, `local_committee_id`, `status_id`, `global_recruitment_id`) VALUES
(1, 1, 1, 2),
(2, 3, 2, 3),
(3, 5, 1, 3),
(4, 6, 1, 2),
(5, 6, 1, 3);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `recruitments_in_current_global_recruitment`
--
CREATE TABLE IF NOT EXISTS `recruitments_in_current_global_recruitment` (
`local_committee_id` int(11)
,`local_committee_name` varchar(64)
,`recruitment_status_id` bigint(11)
,`recruitment_status_name` varchar(64)
,`recruitment_status_human_name` varchar(64)
);
-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `recruitment_status`
--

CREATE TABLE IF NOT EXISTS `recruitment_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_polish_ci NOT NULL,
  `human_name` varchar(64) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `recruitment_status`
--

INSERT INTO `recruitment_status` (`id`, `name`, `human_name`) VALUES
(1, 'closed', 'Zamnięta'),
(2, 'open', 'Otwarta');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `universities`
--

CREATE TABLE IF NOT EXISTS `universities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_polish_ci NOT NULL,
  `local_committee_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=18 ;

--
-- Zrzut danych tabeli `universities`
--

INSERT INTO `universities` (`id`, `name`, `local_committee_id`) VALUES
(1, 'Politechnika Gdańska', 1),
(2, 'Politechnika Częstochowska', NULL),
(3, 'Politechnika Śląska', 6),
(4, 'Akademia Górniczo-Hutnicza', 3),
(5, 'Politechnika Łódzka', 8),
(6, 'Uniwersytet Łódzki', 12),
(7, 'Uniwersytet Medyczny w Łodzi', 7),
(8, 'Politechnika Lubelska', NULL),
(9, 'Uniwersytet Warmińsko-Mazurski', NULL),
(10, 'Politechnika Poznańska', 13),
(11, 'Politechnika Rzeszowska', 14),
(12, 'Zachodniopomorski Uniwersytet Technologiczny', 11),
(13, 'Politechnika Warszawska', 2),
(14, 'Politechnika Wrocławska', 4),
(15, 'Politechnika Krakowska', 5),
(16, 'Uniwersytet Wrocławski', NULL),
(17, 'Szkoła Główna Gospodarstwa Wiejskiego', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `local_committee_id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL DEFAULT '6',
  `name` varchar(64) COLLATE utf8_polish_ci NOT NULL,
  `surname` varchar(64) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `email_2` (`email`),
  KEY `email_3` (`email`),
  KEY `password` (`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=16 ;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `local_committee_id`, `user_role_id`, `name`, `surname`, `email`, `password`, `activated`) VALUES
(1, 1, 7, 'Kamil', 'Sojecki', 'kamil@sojecki.pl', '80978adf3838cd072a0ac269591acd19f3b673a5', 1),
(4, 2, 5, 'Maciek', 'Klan', 'maciekklan@wp.pl', 'c3e563b48f36fd25dd17d162eb8e8b602e6ad9fd', 0),
(5, 1, 1, 'Monia', 'Abramowicz', 'ma@ma.pl', 'c3e563b48f36fd25dd17d162eb8e8b602e6ad9fd', 0),
(6, 1, 3, 'Kamil', 'Sojecki', 'ks@wp.pl', 'a03753772d282034474f928cbc8cfdcc3472b0fa', 0),
(7, 11, 4, 'Wacław', 'Miękki', 'wacek@wacek.pl', '2dd5a343b99c0b16ea24a6a2276d20be619b665f', 0),
(9, 1, 6, 'Kamil', 'Sojecki', 'stefan@sojecki.pl', '80978adf3838cd072a0ac269591acd19f3b673a5', 0),
(10, 1, 6, 'Kamil', 'Sojecki', 'zdzisio@dddd.pl', '80978adf3838cd072a0ac269591acd19f3b673a5', 0),
(11, 1, 6, 'kamil', 's', 'k@w.pl', '99cc6790e3fe46cd66b834877898c3517e45485b', 0),
(12, 6, 6, 'Natalia', 'Krzyżanowska', 'natalia.krzyzanowska@iaeste.pl', 'c3e563b48f36fd25dd17d162eb8e8b602e6ad9fd', 1),
(13, 1, 6, 'Tester', 'Testowy', 'testowy@test.pl', 'e43ff9e6bda9df60bcf0d005835c0435cd4fd34c', 0),
(14, 1, 6, 'Testowy2', 'Tester', 'test@test.com.pl', 'c3e563b48f36fd25dd17d162eb8e8b602e6ad9fd', 0),
(15, 1, 6, 'Test1', 'Test1', 'kupa@wp.pl', 'e43ff9e6bda9df60bcf0d005835c0435cd4fd34c', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_polish_ci NOT NULL,
  `human_name` varchar(64) COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=8 ;

--
-- Zrzut danych tabeli `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`, `human_name`, `description`) VALUES
(1, 'main_coordinator', 'Koordynator główny', ''),
(2, 'local_coordinator', 'Koordynator lokalny', ''),
(3, 'lc_board_member', 'Członek zarządu komitetu lokalnego', ''),
(4, 'national_board_member', 'Członek Biura Narodowego', ''),
(5, 'hr_group_member', 'Członek grupy HR', ''),
(6, 'disabled', 'Wyłączony', ''),
(7, 'admin', 'Administrator', '');

-- --------------------------------------------------------

--
-- Struktura widoku `recruitments_in_current_global_recruitment`
--
DROP TABLE IF EXISTS `recruitments_in_current_global_recruitment`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `recruitments_in_current_global_recruitment` AS select `lc`.`id` AS `local_committee_id`,`lc`.`name` AS `local_committee_name`,ifnull(`recruitments`.`status_id`,1) AS `recruitment_status_id`,`recruitment_status`.`name` AS `recruitment_status_name`,`recruitment_status`.`human_name` AS `recruitment_status_human_name` from ((`local_committees` `lc` left join (`recruitments` join `global_recruitments` on((`recruitments`.`global_recruitment_id` = `global_recruitments`.`id`))) on((`recruitments`.`local_committee_id` = `lc`.`id`))) left join `recruitment_status` on((ifnull(`recruitments`.`status_id`,1) = `recruitment_status`.`id`))) where ((`recruitments`.`global_recruitment_id` = (select max(`recruitments`.`global_recruitment_id`) from `recruitments` where (`recruitments`.`local_committee_id` = `lc`.`id`))) or isnull(`recruitments`.`global_recruitment_id`)) order by `lc`.`name`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
